import statistics
import random
from numpy.random import normal
from pprint import pprint
from enum import Enum

class Round(Enum):
    SEED = 0
    SERIES_A = 1
    SERIES_B = 2
    SERIES_C = 3
    SERIES_D = 4

    def get_model_post_valuation(self):
        # maps the round to the parameters of the normal distribution
        normal_params = {Round.SEED: (6,1),
                         Round.SERIES_A: (22, 2),
                         Round.SERIES_B: (50, 5),
                         Round.SERIES_C: (80, 10),
                         Round.SERIES_D: (140, 20)}
        n = normal_params[self]
        return round(normal(n[0], n[1]))


class State(Enum):
    ALIVE = 0
    SOLD = 1
    DEAD = 2


class Portfolio:
    def __init__(self, investment_capital, followon_capital):
        self.investment_cap = investment_capital
        self.followon_cap = followon_capital
        self.return_cap = 0
        self.companies = []


    def build(self, portfolio_model):
        amt = {Round.SEED: 1,
               Round.SERIES_A: 2,
               Round.SERIES_B: 3.5,
               Round.SERIES_C: 0,
               Round.SERIES_D: 0}

        for t in portfolio_model:
            for deal in range(t[0]):
                c = Company(t[1], amt[t[1]],
                            t[1].get_model_post_valuation())
                self.investment_cap = self.investment_cap - amt[t[1]]
                if self.investment_cap < 0:
                    raise "Bad portfolio construction: Out of investment capital."
                self.companies.append(c)

        self.followon_cap += self.investment_cap
        self.investment_cap = 0


    def return_capital(self, amt):
        self.return_cap += amt

    def ask_for_more(self, company, amt):
        if (self.followon_cap >= amt):
            self.followon_cap -= amt
            return True
        return False

    def advance(self):
        for company in self.companies:
            if (company.state == State.ALIVE):
                company.maybe_advance(self)
        return any(map(lambda c: c.state == State.ALIVE, self.companies))

    def run(self):
        while self.advance():
            pass

    def dump(self):
        for c in self.companies:
            print (c.entry_round, "-->", c.round, "\t", c.state, "\t", c.investment, "\t", c.amount_returned)


class Company:
    def __init__(self, series, investment, post_valuation):
        self.round = series
        self.investment = investment
        self.post_valuation = post_valuation
        self.state = State.ALIVE
        self.entry_round = self.round
        self.amount_returned = 0

        assert post_valuation > investment


    def maybe_advance(self, portfolio):
        if self.state != State.ALIVE:
            return

        # calculate randomly if i'm dead or not
        if self.bankrupt():
            self.state = State.DEAD
            # TODO: later return random capital btwx 0-investment heavily weighted to zero
            portfolio.return_capital(0)
            return

        # assuming not, then choose if i'm selling or another round
        #
        if self.round == Round.SERIES_D:
            selling = True
        else:
            selling = self.maybe_sell()

        if selling:
            sale_scale = round((random.random() * 2.0) + 1.0, 1)
            sale_amount = sale_scale * self.post_valuation
            return_amt = (self.investment / float(self.post_valuation)) * sale_amount
            portfolio.return_capital(max(self.investment, return_amt))
            self.amount_returned = return_amt
            self.state = State.SOLD
            return

        # Advance to the next round:
        self.round = Round(self.round.value + 1)

        (pre_money_valuation, size_of_round, post_money_value) = self.get_round_parameters()
        #
        # ex:  (20, 5 (randomly chosen from range), 25)
        # ex: investment = 2 so therefore
        #   prorata = (2 / 10) * 5

        assert pre_money_valuation + size_of_round == post_money_value

        # TODO: this formula is wrong, the calculation of prorata is wrong after the 1st round
        prorata = round((self.investment / float(self.post_valuation)) * size_of_round, 1)

        self.post_valuation = post_money_value

        if (portfolio.ask_for_more(self, prorata)):
          self.investment += prorata


    def maybe_sell(self):
        return random.randint(0,1) == 1

    def get_round_parameters(self):
        if (self.round == Round.SERIES_A):
            return (20, 5, 25)
        elif (self.round == Round.SERIES_B):
            return (50, 20, 70)
        elif (self.round == Round.SERIES_C):
            return (100, 25, 125)
        elif (self.round == Round.SERIES_D):
            return (200, 50, 250)
        else:
            raise "You suck"

    def bankrupt(self):
        if (self.round == Round.SEED):
            return random.randint(0, 2) == 0
        elif (self.round == Round.SERIES_A):
            return random.randint(0, 3) == 0
        elif (self.round == Round.SERIES_B):
            return random.randint(0, 4) == 0
        elif (self.round == Round.SERIES_C):
            return random.randint(0, 9) == 0
        elif (self.round == Round.SERIES_D):
            return random.randint(0, 19) == 0
        else:
            raise "You suck"


def test():
    ret = []
    for i in range(1000):
        p = Portfolio(30, 10)
        p.build(((4, Round.SEED),
                 (8, Round.SERIES_A),
                 (2, Round.SERIES_B)))
        p.run()
        #p.dump()
        print ("You made ", p.return_cap)
        ret.append(p.return_cap)

    print ("Mean: ", statistics.mean(ret))
    print ("Median: ", statistics.median(ret))
    print ("StdDev: ", statistics.stdev(ret))

    return
    c = Company(Round.SEED, 1, 2)
    while c.state == State.ALIVE:
        c.maybe_advance(p)
        pprint(vars(c))
        pprint(vars(p))

test()
